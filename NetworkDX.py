#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri May 11 20:18:42 2018

@author: mzanin
"""

import datetime
import math
import random
from multiprocessing import Pool, cpu_count
import networkx as nx
import numpy as np
import time


class NodeDoesntExistError(Exception):
    def __init__(self, node):
        self.node = node
        self.message = 'Node "{0}" is not in the graph'.format(node)


class LinkDoesntExistError(Exception):
    def __init__(self, link):
        self.link = link
        self.message = 'Link "{0}" is not in the graph'.format(link)


class LinkAttributeUndefinedError(Exception):
    def __init__(self, attribute_name, node1, node2):
        self.node1 = node1
        self.node2 = node2
        self.attribute_name = attribute_name
        self.message = 'Attribute "{0}" is undefined for Link ({1}, {2})'.format(attribute_name, node1, node2)


class ParameterTypeError(Exception):
    def __init__(self, expected_type, actual_type, parameter_name):
        self.expected_type = expected_type
        self.actual_type = actual_type
        self.parameter_name = parameter_name
        self.message = 'Parameter type mismatch: Expected "{0}", received "{1}" for parameter "{2}"'.format(
            expected_type, actual_type, parameter_name)


# noinspection PyPep8Naming
class NetworkDX:

    def __init__(self, numNodes, load_path=None):
        self.debugModeOn = True
        if load_path is None:
            self.shortestPathsAreUpdated = False  # I initialize with False, because otherwise if there are no edges,
            # calculation will not trigger
            self.topologyEvolution = []
            self.snapshotSimilarity = None
            self.numNodes = numNodes
            self.AM = np.zeros((numNodes, numNodes), dtype=int)
            self.G = nx.from_numpy_matrix(self.AM)
            self.linkDensity = 0.0
            self.allShortestPaths = [[[] for i in range(self.numNodes)] for j in range(self.numNodes)]
            self.emptyShortestPaths = self.allShortestPaths.copy()  # not to have to initialize empty shortest paths
            # matrix every time we calculate
            self.allShortestPathsLengths = np.full((self.numNodes, self.numNodes), np.inf)
            self.emptyShortestPathLengths = self.allShortestPathsLengths.copy()
        else:
            raise Exception("Not Implemented")
            # self.G = nx.read_gml(load_path + ".gml")
            # self.numNodes = self.G.number_of_nodes()
            # self.linkDensity = self.G.number_of_edges() / (self.numNodes ** 2)
            # self.AM = np.load(load_path + ".npy")

    # Auxiliary functions for debugging - start
    def print_nodes(self):
        if not self.debugModeOn:
            return
        print("Current Nodes: \n")
        for n in self.G.nodes:
            print(n, ":", self.G[n], "\n")

    def print_links(self):
        if not self.debugModeOn:
            return
        print("Current Links: \n")
        for link in self.G.edges(data=True):
            print(link)

    def print_paths(self):
        if not self.debugModeOn:
            return
        print("All shortest paths")
        for i in range(self.numNodes):
            for j in range(self.numNodes):
                print("(", i, ",", j, ")", self.allShortestPaths[i][j], '\n')

    def print_debug_msg(self, to_print):
        if self.debugModeOn:
            print(to_print)

    # Auxiliary functions for debugging - end

    def save_network(self, path):
        """saves network object to files: Graph as .gml, matrix as .npy"""
        raise Exception('Not Implemented')
        nx.write_gml(self.G, path + ".gml")
        np.save(path + ".npy", self.AM)

    def generate_random_weighted_links(self, link_probability=0.5):
        """generates random links with random weight. link_probability
        is a number between 0 and 1, which is the probability for each pair of nodes
        that a link will be generated. Default is 0.5"""
        if link_probability < 0.0 or link_probability > 1.0:
            link_probability = 0.5
        for i in range(0, self.numNodes):
            for j in range(0, self.numNodes):
                if i == j:
                    continue
                skip = random.random()
                if skip > link_probability:
                    continue
                random_weight = round(random.uniform(1.5, 500.0), 2)
                self.add_link(i, j, random_weight)

    def node_exists(self, node):
        # since the nodes are all the ints in [0,numNodes-1],
        # this will check if the node parameter  is actually a node / if it exists
        if node < 0 or node >= self.numNodes:
            self.print_debug_msg(str(node) + 'is not a node')
            return False
        return True

    def add_snapshot_callback(self, result):
        """Callback for asynchronous snapshot function. Each tuple in the list is of format
        (start_time, end_time, adjuscency_matrix, link_density, efficiency, diameter)"""
        end_time = datetime.datetime.now()
        self.topologyEvolution.append((result[0], end_time, result[1], result[2], result[3], result[4]))
        self.topologyEvolution.sort(key=lambda r: r[0])

    def add_snapshot_worker(self, passed_AM,passed_G, passed_updated, passed_shortest_paths, passed_shortest_path_lengths,
                            linkDesnity, save_AM, save_density, save_efficiency, save_diameter):
        """The worker function for asynchronous add_snapshot funtion. Updates paths if necessary. Returns
        a tuple (start_time, Adjacency_Matrix, Link_Density, Efficiency, Diameter)"""
        start_time = datetime.datetime.now()
        self.AM = passed_AM
        self.G = passed_G
        self.shortestPathsAreUpdated = passed_updated
        self.allShortestPaths = passed_shortest_paths
        self.allShortestPathsLengths = passed_shortest_path_lengths
        self.linkDensity = linkDesnity
        if not self.shortestPathsAreUpdated:
            self.calculate_shortest_paths()
        efficiency = None
        density = None
        diameter = None
        AM = None
        if save_efficiency:
            efficiency = self.network_efficiency()
        if save_density:
            density = self.linkDensity
        if save_diameter:
            diameter = self.network_diameter()
        if save_AM:
            AM = passed_AM
        return start_time, AM, density, efficiency, diameter

    def add_snapshot(self, save_AM=True, save_density=True, save_efficiency=True, save_diameter=True):
        """asynchronous function for getting efficiency snapshot"""
        pool = Pool(processes=cpu_count())
        passed_shortest_paths = None
        passed_shortest_path_lengths = None
        passed_AM = np.copy(self.AM)
        passed_updated = self.shortestPathsAreUpdated
        passed_G = self.G.copy()
        if passed_updated:
            passed_shortest_paths = self.allShortestPaths.copy()
            passed_shortest_path_lengths = np.copy(self.allShortestPathsLengths)
        snapshot_result = pool.apply_async(self.add_snapshot_worker,
                                           [passed_AM, passed_G, passed_updated, passed_shortest_paths, passed_shortest_path_lengths,
                                            self.linkDensity, save_AM, save_density, save_efficiency, save_diameter],
                                           callback=self.add_snapshot_callback)
        pool.close()

    def shortest_path_callback(self, result):
        self.allShortestPaths = result[0]
        self.allShortestPathsLengths = result[1]
        self.set_shortest_path_flag(True)

    def calculate_shortest_paths_worker(self):
        # get the shortest paths for all pairs using networkx function and cast it as dict
        self.allShortestPaths = self.emptyShortestPaths.copy()  # to retain empty lists
        self.allShortestPathsLengths = self.emptyShortestPathLengths.copy()
        current_shortest_paths = dict(nx.all_pairs_dijkstra_path(self.G))
        for source, value in current_shortest_paths.items():
            for dest, path in value.items():
                self.allShortestPaths[source][dest] = path
                self.allShortestPathsLengths[source][dest] = self.calculate_path_length(
                    current_shortest_paths[source][dest])
        return self.allShortestPaths, self.allShortestPathsLengths

    def calculate_shortest_paths(self, asynchronous=False):
        """calculates all the shortest paths in the Graph using networkx built-in all_pairs_dijkstra_path function.
        Doesn't return anything. The values are stored in a matrix, source being the row, destination being the column and list of nodes
         (the path) as value. If no path exists, empty list will be assigned"""
        if asynchronous:
            pool = Pool(processes=cpu_count())
            calculation_result = pool.apply_async(self.calculate_shortest_paths_worker, [],
                                                  callback=self.shortest_path_callback)
            pool.close()
        else:
            result = self.calculate_shortest_paths_worker()
            self.allShortestPaths = result[0]
            self.allShortestPathsLengths = result[1]
            self.set_shortest_path_flag(True)

    def calculate_snapshot_similarity_callback(self, result):
        self.snapshotSimilarity = result

    def calculate_snapshot_similarity_worker(self):
        start_index = 0
        result = np.zeros((len(self.topologyEvolution), len(self.topologyEvolution)), dtype=float)
        if self.snapshotSimilarity is not None:
            start_index = self.snapshotSimilarity.shape[0]
            result[0:start_index, 0:start_index] = self.snapshotSimilarity
        for i in range(start_index, len(self.topologyEvolution)):
            if self.topologyEvolution[i][2] is None:
                result[i, :] = np.nan  # if we didn't save the adjacency matrix at that snapshot, we assign nans
                result[:, i] = np.nan  # the matrix is symmetric, so we can do it for the reverse as well
                continue
            for j in range(0, len(self.topologyEvolution)):
                if self.topologyEvolution[j][2] is None:
                    result[i, j] = math.nan
                    result[j, i] = math.nan
                    continue
                if i == j:
                    result[i, j] = 1.0
                    continue
                same_count = np.sum(self.topologyEvolution[i][2] == self.topologyEvolution[j][2])
                similarity = same_count / (self.numNodes * self.numNodes)
                result[i, j] = similarity
        return result

    def calculate_snapshot_similarity(self):
        """calculates adjacency matrix similarities for all snapshots"""
        pool = Pool(processes=cpu_count())
        calculation_result = pool.apply_async(self.calculate_snapshot_similarity_worker, [],
                                              callback=self.calculate_snapshot_similarity_callback)
        pool.close()

    def set_shortest_path_flag(self, value):
        """Sets "shortestPathsAreUpdated" flag to the passed boolean value"""
        if type(value) is not bool:
            return
        self.shortestPathsAreUpdated = value

    def calculate_path_length(self, path):
        """calculates the length of a path using 'weight' attribute.
        path should be a list of nodes. If the path is not a list, the list is empty,
        a node or an edge does not exist in the network, or the weight attribute is absent,
        returns None"""
        # couldn't find an built-in algorithm, so I'm dong it manually:

        # I did this dynamically because we decided not to have shortest path lengths stored
        path_weight = 0
        if path is None:
            self.print_debug_msg("path is none")
            return None
        elif type(path) is not list:
            self.print_debug_msg("path is not a list")
            raise ParameterTypeError(expected_type='list', actual_type=type(path), parameter_name='path')
            return None
        elif len(path) == 0:
            self.print_debug_msg("path is empty")
            return None
        elif not self.node_exists(path[0]):  # this will check if the first elemnt is a node
            self.print_debug_msg("1st node doesn't exist")
            raise NodeDoesntExistError(path[0])
            return None
        elif len(path) == 1:  # if there is only one element, the length is 0 by default
            return 0

        for i in range(0, len(path) - 1):
            if not self.node_exists(path[i + 1]):
                # check if "second" node not in the graph
                # the first node was checked before, here we check second of each pairs
                # thus, all the nodes will checked once
                self.print_debug_msg("node " + str(path[i + 1]) + " does not exist")
                raise NodeDoesntExistError(path[i + 1])
                return None
            elif path[i] == path[i + 1]:
                continue
            elif self.AM[path[i]][path[i + 1]] == 0:
                # if there is no edge, return None. mabe return math.inf would be better?
                self.print_debug_msg("no link between " + str(path[i]) + " and " + str(path[i + 1]))
                return None
            elif 'weight' not in self.G[path[i]][path[i + 1]]:
                # maybe we should somehow enforce there is always a 'weight' parameter, thus avoid this check
                self.print_debug_msg("weight not defined")
                raise LinkAttributeUndefinedError("weight", path[i], path[i + 1])
                return None
            edge_weight = self.G[path[i]][path[i + 1]]['weight']
            path_weight += edge_weight
        return path_weight

    def shortest_path(self, source, destination):
        """returns the shortest path between two nodes.
        source and destination nodes act as arguments. if any of them is None, raises
        an exception. If there is no path between these nodes, returns empty list"""
        if type(source) is not int:
            raise ParameterTypeError("int", type(source), "source")
        if type(destination) is not int:
            raise ParameterTypeError("int", type(destination), "destination")
        if not self.shortestPathsAreUpdated:
            self.calculate_shortest_paths()
        return self.allShortestPaths[source][destination]

    def get_shortest_path_length(self, source, destination):
        """calculates the length of the shortest path between two nodes.
        source and destination nodes act as arguments. if any of them is None, raises
        an exception. If there is no path between these nodes, returns None"""
        if type(source) is not int:
            raise ParameterTypeError("int", type(source), "source")
        if type(destination) is not int:
            raise ParameterTypeError("int", type(destination), "destination")
        if not self.shortestPathsAreUpdated:
            self.calculate_shortest_paths()
        return self.allShortestPathsLengths[source][destination]

    def add_link(self, node1, node2, edge_weight):
        if type(edge_weight) is not float and type(edge_weight) is not int:
            raise ParameterTypeError("int or float", type(edge_weight), "edge_weight")
        elif type(node1) is not int:
            raise ParameterTypeError("int", type(node1), "node1")
        elif type(node2) is not int:
            raise ParameterTypeError("int", type(node2), "node2")
        elif not self.node_exists(node1):
            raise NodeDoesntExistError(node=node1)
        elif not self.node_exists(node2):
            raise NodeDoesntExistError(node=node2)
        if type(edge_weight) is int:
            edge_weight = float(edge_weight)
        if self.AM[node1, node2] == 0:
            self.AM[node1, node2] = 1
            self.AM[node2, node1] = 1  # the Graph we use is bi-directional, so without this, there were errors
            self.G.add_edge(node1, node2)
            if edge_weight is not None:
                if edge_weight <= 0:
                    edge_weight = 1.0  # only non-negatives allowed. For now, just assign 1. Later - maybe an exception?
                self.G[node1][node2]['weight'] = edge_weight
            self.linkDensity += 1.0 / (self.numNodes ** 2)
            self.set_shortest_path_flag(False)

    def remove_link(self, node1, node2):

        if self.AM[node1, node2] == 1:
            self.AM[node1, node2] = 0
            self.G.remove_edge(node1, node2)
            self.linkDensity -= 1.0 / (self.numNodes ** 2)
            self.set_shortest_path_flag(False)

    def change_weight(self, node1, node2, edge_weight):
        if type(edge_weight) is not float and type(edge_weight) is not int:
            raise ParameterTypeError("int or float", type(edge_weight), "edge_weight")
        elif type(node1) is not int:
            raise ParameterTypeError("int", type(node1), "node1")
        elif type(node2) is not int:
            raise ParameterTypeError("int", type(node2), "node2")
        elif self.AM[node1, node2] == 1 and edge_weight > 0:
            self.G[node1][node2]['weight'] = edge_weight
            self.set_shortest_path_flag(False)
        else:
            pass
            # maybe raise an Exception or just call add_edge function

    def network_efficiency(self):
        """Calculates efficiency using formula: 1/(N*(N-1)) * SUM ( 1/shortest_distance(i,j) ); i!=j; i,j>0 and i,
        j < N """
        if not self.shortestPathsAreUpdated:
            self.calculate_shortest_paths()
        path_weight_inverse_sum = 0
        for i in range(0, self.numNodes):
            for j in range(0, self.numNodes):
                if i == j:
                    continue
                current_path_length = self.allShortestPathsLengths[i][j]
                if current_path_length == 0:
                    raise Exception(
                        "current_path_length should not be 0")  # this should never happen according to the code
                if current_path_length is None:
                    raise Exception("something went wrong")  # should think of something better to do
                path_weight_inverse_sum += 1 / current_path_length
        return path_weight_inverse_sum / (self.numNodes * (self.numNodes - 1))

    def node_eccentricity(self, node):
        """Given a node, it returns the maximal existing distance value, determined by weights. Infinite values are
        ignored """
        if not self.shortestPathsAreUpdated:
            self.calculate_shortest_paths()
        if type(node) is not int:
            raise ParameterTypeError("int", type(node), "node")
        if not self.node_exists(node):
            raise NodeDoesntExistError(node=node)
        current_path_lengths = self.allShortestPathsLengths[node]
        return np.nanmax(current_path_lengths[current_path_lengths != np.inf])  # this ignores infinite values

    def network_diameter(self):
        """returns maximal distance between any two nodes. Infinite values are ignored"""
        if not self.shortestPathsAreUpdated:
            self.calculate_shortest_paths()
        return np.nanmax(
            self.allShortestPathsLengths[self.allShortestPathsLengths != np.inf])  # this ignores infinite values
