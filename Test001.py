#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri May 11 20:18:42 2018

@author: mzanin
"""

import numpy as np
import networkx as nx
import random
import NetworkDX as ndx
import time

outerLoops = 10

allRes = np.zeros((2, outerLoops))

for oL in range(outerLoops):

    print('Iteration: A, %d' % oL)
    numNodes = 100

    innerLoops = 10

    startT = time.time()

    for iL in range(innerLoops):

        myDX = ndx.NetworkDX(numNodes=numNodes)
        for k in range(10, int(numNodes * numNodes / 4.0)):
            myDX.add_link(np.random.randint(numNodes), np.random.randint(numNodes), 1.0)

        myDX.calculate_shortest_paths(asynchronous=False)
        for k in range(10):
            efficiency = myDX.network_efficiency()
            diameter = myDX.network_diameter()

    allRes[0, oL] = (time.time() - startT) / innerLoops

for oL in range(outerLoops):

    print('Iteration: B, %d' % oL)
    numNodes = 100

    innerLoops = 10

    startT = time.time()

    for iL in range(innerLoops):

        myNX = nx.Graph()
        for k in range(1, int(numNodes * numNodes / 4.0)):
            myNX.add_edge(np.random.randint(numNodes), np.random.randint(numNodes))

        for k in range(2):
            efficiency = nx.global_efficiency(myNX)
            diameter = nx.diameter(myNX)

    allRes[1, oL] = (time.time() - startT) / innerLoops

print(allRes)
