#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri May 11 20:18:42 2018

@author: mzanin
"""

import numpy as np
import networkx as nx
import random
import NetworkDX as ndx
import time

# ---------------------------------------------------------------
#   Test 1
#   Simple analysis of shortest paths
#
if __name__ == '__main__':
    print('\n> Executing Test 1')

    try:
        myDX = ndx.NetworkDX(numNodes=100)

        for k in range(1, 100):
            myDX.add_link(k - 1, k, 1.0)

            distance = myDX.get_shortest_path_length(0, k)
            if distance != k:
                print('Test 1: Error found in shortest path length, iteration %d' % k)

        myDX.add_link(99, 0, 1.0)

        diameter = myDX.network_diameter()
        if diameter != 50:
            print('Test 1: Error found in the diameter of the network')

    except Exception as e:
        print('Exception found in test 1')
        print(e)

    # ---------------------------------------------------------------
    #   Test 2
    #   Check if shortest paths are correctly updated in the asynchronous version
    #
    print('\n> Executing Test 2')

    try:
        myDX = ndx.NetworkDX(numNodes=100)

        for k in range(1, 100):
            myDX.add_link(k - 1, k, 1.0)
        myDX.add_link(99, 0, 1.0)

        myDX.calculate_shortest_paths(asynchronous=True)
        if not np.isinf(myDX.allShortestPathsLengths[0, 99]):
            print('Test 2: Distance should be infinite')

        time.sleep(2.0)

        if myDX.allShortestPathsLengths[0, 99] != 1.0:
            print('Test 2: Distance should be 1.0')

    except Exception as e:
        print('Exception found in test 2')
        print(e)

    # ---------------------------------------------------------------
    #   Test 3
    #   Check of the network snapshots
    #
    print('\n> Executing Test 3')

    try:
        myDX = ndx.NetworkDX(numNodes=100)

        for k in range(3, 100):
            myDX.add_link(k - 3, k, 1.0)
        myDX.add_link(99, 3, 1.0)

        for loop in range(5):

            myDX.add_snapshot()

            for k in range(1, 100):
                myDX.add_link(k - 1, k, 1.0)
            myDX.add_link(99, 0, 1.0)

            myDX.add_snapshot()

            for k in range(1, 100):
                myDX.remove_link(k - 1, k)
            myDX.remove_link(99, 0)

        if len(myDX.topologyEvolution) > 0:
            print('Test 3: Initial Efficiency evolution should be empty')

        time.sleep(15)

        print(myDX.topologyEvolution)




    except Exception as e:
        print('Exception found in test 3')
        print(e)
